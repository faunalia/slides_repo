# Faunalia

## una presentazione dell'azienda

![Logo](assets/logo_faunalia.png)

---

## Origini

- **Faunalia 0.0** (2000-2005): gruppo informale di professionisti; principalmente consulenze faunistiche ed ambientali
- **Faunalia 1.0** (2006-2009): *Associazione Professionale*; consulenze faunistiche e crescente attività GIS
- **Faunalia 2.0** (2010-2018): *Associazione Professionale*; 100% GIS development, support & training
- **Faunalia 3.0** (2018-): *Srl*; stesse attività, maggiore flessibilità amministrativa

---

## Struttura

- Faunalia 2.0 e 3.0: struttura leggera, ricco network di risorse attivate in funzione del progetto specifico.
- Collaborazione stabile con gli sviluppatori principali di tutti i progetti su cui lavoriamo:

    - QGIS, PostGIS, GDAL, GRASS, Lizmap

- Forte investimento nella comunità:
    - partecipazione a steering committee, hackfest, ecc.

---

## Vantaggi della struttura

- velocità ed efficienza di implementazione:
  - atttiviamo direttamente chi ha implementato una specifica parte di codice
- facile scalabilità

---

## Attività

- **sviluppo**: funzioni core, plugins, bugfixing per QGIS, PostGIS, GDAL ed altro
- **supporto**: telefonico, email, onsite per tutti gli stessi software
- **formazione**: oltre 300 corsi, seminari e workshops; oltre 4.500 persone formate

---

## Strumenti principali

- sistema operativo: **Debian**
- sistema GIS: **QGIS** e librerie (**GDAL** ecc.)
- geodatabase: **PostGIS**
- webGIS: **Lizmap**

---

### Alcuni dei progetti più importanti

#### Regione Toscana
- supporto alla creazione dell'infrastruttura di gestione dati
- funzioni core e plugins per QGIS
- funzioni core per PostGIS, soprattutto per la parte legata alla topologia

---

### Alcuni dei progetti più importanti

#### ENEL
*(direttamente e tramite Ericsson, Italdata e Indra)*

- implementazione di un sistema GIS globale per l'azienza e le sue controllate (ENDESA etc.)
- basato su QGIS core con plugins ad hoc
- backend Oracle
- su infrastruttura cloud Amazon
- velocizzazione del provider Oracle di 1.5 ordini di grandezza
- ambiente di editing altamente strutturato, con regole logiche e topologiche, gestione del versioning nativo in Oracle
- strumenti specializzati per la stampa, la gestione degli indirizzi ecc.

---

## Altri clienti

- le maggiori Regioni italiane, e le relative Agenzie ambientali
- Università ed enti di ricerca italiani, europei ed extra-europei
- molte aziende di vari settori (ambientale, energia, aerospaziale, assicurativo e finanziario, public utilities ecc.)
- organizzazioni internazionali (FAO, WHO, WWF)

---

## Licence with image

Creative Commons
Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0)

![](https://upload.wikimedia.org/wikipedia/commons/d/d0/CC-BY-SA_icon.svg)

* More info: http://creativecommons.org/licenses/by-sa/3.0/
