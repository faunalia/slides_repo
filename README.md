# Generic Container of Faunalia Slides

Using [https://gitpitch.com/](https://gitpitch.com/) as backend to build the 
slides.

The documentation is [here](https://gitpitch.com/docs/about/)

Each topic is on a different branch, easily reachable by changing the URL, e.g.

https://gitpitch.com/faunalia/slides_repo/master?grs=gitlab

where:

* `faunalia` is gitlab account
* `slides_repo` is the repository
* `master` is the branch
* `?grs=gitlab` is the suffix for gitlab

## Workflow

Basically the whole presentation is contained in the `PITCHME.md` file. 
The `PITCHME.yaml` file contain some custom configuration for the presentation.

Of course `PITCHME.md` file must be changed for each different prentation while 
`PITCHME.yaml` can be left as it is for all branches.

In order to have a clean structure, images are contained in the `assets` folder.

## Tips&Tricks

Syntax is Markdown, see [here](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
for a cheatsheet.

See the [gitpitch docs](https://gitpitch.com/docs/about/) for detailed explanation,
but basically, some tips:

* each slide is separated by 3 `-` to scroll horizontally and 3 `+` to scroll vertically
* to have an image as a fragment between 2 slides just use something like `---?image=assets/image_name.png&size=contain`
* to change color of list items (useful to focus on a specific item) use `span`
  <span style="color:gray">Blablabla</span>
  
## Create a new presentation

Checkout the `template` branch and create anther branch from this one:

* `git checkout template`
* `git checkout -b new_presentation`

Edit the `PITCHME.md` (and optionally the `PITCHME.yaml` file) with the new
contents.
